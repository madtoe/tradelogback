package pl.madtoe.tradeLog.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ComponentScan("pl.madtoe.tradeLog")
@EnableWebMvc
@EnableTransactionManagement
@EnableScheduling
@EnableAsync
public class MvcConfig extends WebMvcConfigurerAdapter{
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/tradingSession/**")
                .addResourceLocations("classpath:/resources/templates");
    }

    //todo @Scheduled cron | initial delay?
    //todo wątki async
    //todo local storage
    //todo Service Scope WebAppContext.scope_session
    //proxymode ScoPromo.Target_Class
    //smtp2go?
    //todo dodatkowy komentarz do mejla
    //todo uwzględniać rolę np. admina
    //todo ssh
    //todo slack integration

}
