package pl.madtoe.tradeLog.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private DataSource dataSource;


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("user1")
//                .password("1234")
//                .roles("admin", "user")
//                .and()
//                .withUser("user2").password("1234").roles("user");

        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("select username, password, 1 from trade_log_user where username=?")
                .authoritiesByUsernameQuery("select u.id, r.name from trade_log_user u " +
                        "join user_role_trade_log_user rl on u.id=rl.trade_log_user_id " +
                        "join user_role r on r.id=rl.user_role_id " +
                        "where u.username=?") //.passwordEncoder()
                .passwordEncoder(encoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/user/add/*").hasAuthority("admin")
                .antMatchers("/tradingSession/*/**").authenticated()
                .antMatchers("/*").permitAll()
//                .antMatchers("trade/1").hasRole("admin")
//                .antMatchers("/1").hasAuthority("user")
//                .and().formLogin()
//                .loginProcessingUrl("/logon")
//                .loginPage("/loginform")
//                .defaultSuccessUrl("/trade/newtrade")
//                .failureUrl("/failure").usernameParameter("username")

                .and().formLogin()
                .loginPage("/user/login")
//                .loginProcessingUrl("/user/logProcess")
                .defaultSuccessUrl("/user/toOverview")
                .failureUrl("/user/loginproblem")
                .and().logout()
                .logoutUrl("/user/logout")
                .logoutSuccessUrl("/user/login")
                .and().exceptionHandling()
                .accessDeniedPage("/user/login")
        ;
    }
}
