package pl.madtoe.tradeLog.service;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.madtoe.tradeLog.dao.IUserDAO;
import pl.madtoe.tradeLog.dao.IUserRoleDAO;
import pl.madtoe.tradeLog.model.TradeLogUser;
import pl.madtoe.tradeLog.model.TradingSession;
import pl.madtoe.tradeLog.model.UserRole;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    IUserDAO userDAO;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    IUserRoleDAO userRoleDAO;

    @Autowired
    SessionFactory sessionFactory;

    //todo walidacja np. 2 takiesame name/email etc.

    @Transactional
    public void addUser(String name, String password, String email, String rolename) {

        List<UserRole> roles = new ArrayList<>();
        UserRole userRole = new UserRole();
        userRole.setName(rolename);
        roles.add(userRole);

        userRoleDAO.save(userRole);


        TradeLogUser tradeLogUser = TradeLogUser.builder()
                .email(email)
                .name(name)
                .password(passwordEncoder.encode(password))
                .userRoleList(roles)
                .build();


        userDAO.save(tradeLogUser);
    }


    @Transactional
    public TradeLogUser findLoggedUser(Principal principal) {
        Optional<TradeLogUser> user = userDAO.findFirstByName(principal.getName());
        TradeLogUser tradeLogUser;
        if (user.isPresent()) {
            tradeLogUser = user.get();
            return tradeLogUser;
        }
        return null;
    }

    @Transactional
    public List<TradingSession> findUserSessions(Principal principal) {
        TradeLogUser loggedUser = findLoggedUser(principal);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM TradingSession WHERE userId = :id")
                .setParameter("id", loggedUser.getId());
        List queryList = query.list();
        List<TradingSession> tradingSessionList = new LinkedList<>();
        for (final Object o : queryList) {
            tradingSessionList.add((TradingSession) o);
        }
        return tradingSessionList;
    }

    @Transactional
    public ArrayList<TradeLogUser> getTradeLogUsers() {
        Iterable<TradeLogUser> all = userDAO.findAll();
        ArrayList<TradeLogUser> users = new ArrayList<>();
        for (TradeLogUser t: all) {
            users.add(t);
        }
        return users;
    }

}
