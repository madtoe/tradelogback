package pl.madtoe.tradeLog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.madtoe.tradeLog.dao.IScreenshotDAO;
import pl.madtoe.tradeLog.dao.ITradeDAO;
import pl.madtoe.tradeLog.dao.ITradingSessionDAO;
import pl.madtoe.tradeLog.dao.IUserDAO;
import pl.madtoe.tradeLog.model.Screenshot;
import pl.madtoe.tradeLog.model.Trade;
import pl.madtoe.tradeLog.model.TradeLogUser;
import pl.madtoe.tradeLog.model.TradingSession;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Component
public class TradingSessionService {
    @Autowired
    IScreenshotDAO screenshotDAO;
    @Autowired
    ITradingSessionDAO tradingSessionDAO;
    @Autowired
    ITradeDAO tradeDAO;
    @Autowired
    IUserDAO userDAO;
    @Autowired
    Environment env;

    @Transactional
    public TradingSession createSession(Principal principal) {
        TradingSession tradingSession = new TradingSession();
        tradingSessionDAO.save(tradingSession);
        Optional<TradeLogUser> user = userDAO.findFirstByName(principal.getName());
        TradeLogUser tradeLogUser = new TradeLogUser();
        if (user.isPresent()) {
            tradeLogUser = user.get();
            tradingSession.setUserId(tradeLogUser.getId());
        }
        Trade trade = Trade.builder()
                .session(tradingSession)
                .screenshots(new ArrayList<>())
                .market(tradingSession.getMarket())
                .creationDate(Date.valueOf(LocalDate.now()))
                .tradeStartDate(Timestamp.valueOf(LocalDateTime.now()))
                .isEvaluated(false)
                .build();

        tradeDAO.save(trade);
        tradingSessionDAO.save(tradingSession);
        return tradingSession;
    }

    @Transactional
    public Trade findLastTrade(Long tradingSessionId) {
        Optional<TradingSession> firstById = tradingSessionDAO.findFirstById(tradingSessionId);
        TradingSession tradingSession;
        Trade trade;
        if (firstById.isPresent()) {
            tradingSession = firstById.get();
            List<Trade> tradeList = tradingSession.getTradeList();
            trade = tradeList.get(tradeList.size() - 1);
            return trade;
        }
        return null;
    }


    @Transactional
    public boolean checkOwnership(Principal principal, Long sessionId) {
        String principalName = principal.getName();
        Optional<TradeLogUser> possibleUser = userDAO.findFirstByName(principalName);
        TradingSession session = tradingSessionDAO.findOne(sessionId);
        return null != session && possibleUser.isPresent() && possibleUser.get().getName().equals(principalName);
    }

    @Transactional
    public MultipartFile uploadScreenshot(Long id, MultipartFile screenshot) {
        Screenshot scr = new Screenshot();
        scr.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));
        TradingSession tradingSession = tradingSessionDAO.findOne(id);
        try {
            byte[] bytes = screenshot.getBytes();

            String filename = "tss_" + id + "_scr_" + (tradingSession.getScreenshotList().size() + 1) + "."
                    + screenshot.getOriginalFilename().substring(screenshot.getOriginalFilename().length() - 3);
            scr.setName(filename);
            Path path = Paths.get(env.getProperty("uploadfolder") + "/" + id + "/" + filename);
            new File(env.getProperty("uploadfolder") + "/" + id + "/").mkdir();
            scr.setPath("/tradingSession/download/" + id + "/" + filename + "/");
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Trade trade : tradingSession.getTradeList()) {
            scr.getTradeList().add(trade);
        }
        scr.setSession(tradingSession);
        screenshotDAO.save(scr);
        tradingSessionDAO.save(tradingSession);

        return screenshot;

    }

    @Transactional
    public Trade addNewTradeToTradingSession(Long id) {
        TradingSession tradingSession = tradingSessionDAO.findOne(id);
        Trade lastTrade = findLastTrade(id);
        Trade thisTrade = Trade.builder()
                .session(tradingSession)
                .screenshots(new ArrayList<>())
                .market(lastTrade.getMarket())
                .creationDate(Date.valueOf(LocalDate.now()))
                .tradeStartDate(Timestamp.valueOf(LocalDateTime.now()))
                .timeScale(lastTrade.getTimeScale())
                .isEvaluated(false)
                .build();

        tradeDAO.save(thisTrade);
        return thisTrade;
    }

}
