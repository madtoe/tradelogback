package pl.madtoe.tradeLog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendEmail(String email, String subject, String message) {

        javaMailSender.send(new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

                try {
                    helper.setTo(email);
                    helper.setFrom("tradelog@civic5g.pl", "TradeLog Message");
                    helper.setSubject(subject);
                    helper.setText(message);
                    helper.setText("<html><body><b>BlackWolf: </b>" + message + "</body></html>", true);
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
