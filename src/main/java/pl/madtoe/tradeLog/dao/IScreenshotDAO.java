package pl.madtoe.tradeLog.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.madtoe.tradeLog.model.Screenshot;

@Repository
public interface IScreenshotDAO extends CrudRepository<Screenshot, Long> {
}
