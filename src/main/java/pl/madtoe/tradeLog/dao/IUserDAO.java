package pl.madtoe.tradeLog.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.madtoe.tradeLog.model.TradeLogUser;

import java.lang.management.OperatingSystemMXBean;
import java.util.Optional;

@Repository
public interface IUserDAO extends CrudRepository <TradeLogUser, Long> {

    public Optional<TradeLogUser> findFirstByName(String name);


}
