package pl.madtoe.tradeLog.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.madtoe.tradeLog.model.UserRole;

@Repository
public interface IUserRoleDAO extends CrudRepository <UserRole, Long> {
}
