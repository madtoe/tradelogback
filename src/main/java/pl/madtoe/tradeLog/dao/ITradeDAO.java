package pl.madtoe.tradeLog.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.madtoe.tradeLog.model.Trade;

@Repository
public interface ITradeDAO extends CrudRepository<Trade, Long> {
}
