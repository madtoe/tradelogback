package pl.madtoe.tradeLog.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.madtoe.tradeLog.model.TradingSession;

import java.util.Optional;

@Repository
public interface ITradingSessionDAO extends CrudRepository<TradingSession, Long> {
    public Optional<TradingSession> findFirstById(long id);

}
