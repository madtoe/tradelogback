package pl.madtoe.tradeLog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.madtoe.tradeLog.service.UserService;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class TradeLogApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradeLogApplication.class, args);
	}

	@Autowired
	UserService userService;

	@PostConstruct
	public void addAddmin() {
		userService.addUser("Logan", "admin", "logan4444@gmail.com", "admin");
		userService.addUser("Mark", "halo", "mrkdbsc@gmail.com", "admin");
		userService.addUser("Kretson", "kretson", "krretson@gmail.com", "user");
		userService.addUser("trdlog", "trdlog", "trdlog@civic5g.pl", "user");
	}
}
