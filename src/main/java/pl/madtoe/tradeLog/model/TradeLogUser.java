package pl.madtoe.tradeLog.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TradeLogUser {
// todo logowanie/ poziomy, sesje ->  zaimplementować

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "username")
    private String name;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @ManyToMany
    @JoinTable (name = "user_role_trade_log_user",
            joinColumns = {@JoinColumn(name = "trade_log_user_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_role_id")}
    )
    private List<UserRole> userRoleList;

}
