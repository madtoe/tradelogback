package pl.madtoe.tradeLog.model;

public enum  TimeScale {

    M1, M2, M3, M5, M15, H1, H4, D1, D7
}
