package pl.madtoe.tradeLog.model;


import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "screenshot")
public class Screenshot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private TradingSession session;

    @Column(name = "time_scale")
    private TimeScale timeScale;

    @Column(name = "file_name")
    private String name;

    @Column(name = "file_path")
    private String path;

    @Column(name = "creationDate")
    private Timestamp creationDate;

    @ManyToMany
    @JoinTable (name = "trade_screenshot",
            joinColumns = {@JoinColumn(name = "trade_id")},
            inverseJoinColumns = {@JoinColumn(name = "screenshot_id")}
    )
    private List<Trade> tradeList;

    @OneToMany(mappedBy = "screenshot")
    private List<Marker> markers;

    @Column(name = "commentary")
    private String commentary;

    public Screenshot() {
        markers = new ArrayList<>();
        tradeList = new ArrayList<>();
    }
}
