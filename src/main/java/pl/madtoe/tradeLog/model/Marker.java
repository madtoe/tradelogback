package pl.madtoe.tradeLog.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "marker")
public class Marker {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "marker_number")
    private int markerNumber;

    @Column(name = "marker_axisx")
    private int xAxis;

    @Column(name = "marker_axisy")
    private int yAxis;

    @Column(name = "marker_tag")
    @Enumerated(EnumType.STRING)
    private Tag tag;

    @Column(name = "marker_text")
    private String text;

    @ManyToOne
    private Screenshot screenshot;


}