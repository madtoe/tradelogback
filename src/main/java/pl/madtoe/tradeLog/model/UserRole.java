package pl.madtoe.tradeLog.model;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;


}
