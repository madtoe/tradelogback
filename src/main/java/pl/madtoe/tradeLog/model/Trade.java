package pl.madtoe.tradeLog.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "trade")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Trade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @ManyToOne
    private TradingSession session;

    @Column(name = "time_scale")
    private TimeScale timeScale;

    @Column(name = "trade_startdate")
    private Timestamp tradeStartDate;

    @Column(name = "trade_finishdate")
    private Timestamp tradeEndDate;

    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "trade_score")  //wynik trade'a
    private int score;

    @OneToOne
    private Evaluation evaluation;

    @Column(name = "trade_is_evaluated")
    private boolean isEvaluated;

    @Column(name = "trade_market")
    @Enumerated(EnumType.STRING)
    private Market market;

    @Column(name = "trade_type")
    TradeType type;

    @ManyToMany(mappedBy = "tradeList")
    private List<Screenshot> screenshots;

}
