package pl.madtoe.tradeLog.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "evaluation")
public class Evaluation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "evaluation")
    private Trade trade;

    @Column(name = "background")
    private boolean background;

    @Column(name = "stop_loss")
    private boolean stopLoss;

    @Column(name = "first_evaluation")
    private boolean firstEvaluation;

    @Column(name = "execution")
    private boolean execution;

    @Column(name = "trade_type")
    @Enumerated(EnumType.STRING)
    private TradeType tradeType;

    @Column(name = "scenario")
    private double scenario;

    @Column(name = "last_evaluation")
    private String lastEvaluation;
}
