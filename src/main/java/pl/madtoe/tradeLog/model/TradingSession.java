package pl.madtoe.tradeLog.model;


import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "trading_session")
public class TradingSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "session")
    private List<Trade> tradeList;

    @OneToMany(mappedBy = "session")
    private List<Screenshot> screenshotList;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "creation_date")
    private Timestamp creationDate;

//    @Column(name = "session_startdate")
//    private Timestamp startDate;
//
//    @Column(name = "session_finishdate")
//    private Timestamp endDate;

    @Enumerated(EnumType.STRING)
    Market market;

    @Column(name = "market_scale")
    @Enumerated(EnumType.STRING)
    TimeScale marketScale;

    @Column(name = "session_scale")
    TimeScale sessionScale;

    @Column(name = "session_score")
    private int sessionScore; // todo: auto update na podstawie tradescore s

    @Column(name = "session_evaluated")
    private boolean isEvaluated;

    public TradingSession() {

        this.creationDate = Timestamp.valueOf(LocalDateTime.now());
        this.tradeList = new ArrayList<>();
        this.screenshotList = new ArrayList<>();
    }
}
