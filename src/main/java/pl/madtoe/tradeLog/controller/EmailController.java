package pl.madtoe.tradeLog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import pl.madtoe.tradeLog.dao.IUserDAO;
import pl.madtoe.tradeLog.model.TradeLogUser;
import pl.madtoe.tradeLog.service.EmailService;
import pl.madtoe.tradeLog.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Optional;

@Controller
@RequestMapping("/email")
public class EmailController {

    @Autowired
    EmailService emailService;

    @Autowired
    UserService userService;

    @Autowired
    IUserDAO userDAO;

    @RequestMapping("/sent")
    @ResponseBody
    public String sendEmail(@RequestParam("email") String email,
                                  @RequestParam("subject") String subject,
                                  @RequestParam("message") String message/*,
    HttpServletRequest request*/) {
        emailService.sendEmail(email, subject, message);
//        String whereYouCameFrom = request.getHeader("Referer");
//        return "redirect:" + whereYouCameFrom;
        return "sent!:)";

    }

    @GetMapping("/sendEmail")
    public String emailForm(ModelMap modelMap) {
        modelMap.addAttribute("users", userService.getTradeLogUsers());
        return "sendEmail";
    }


}
