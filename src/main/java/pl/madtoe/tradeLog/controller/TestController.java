package pl.madtoe.tradeLog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.madtoe.tradeLog.dao.IScreenshotDAO;
import pl.madtoe.tradeLog.dao.ITradeDAO;
import pl.madtoe.tradeLog.model.Screenshot;
import pl.madtoe.tradeLog.model.Trade;

import javax.transaction.Transactional;

@Controller
public class TestController {

    @Autowired
    ITradeDAO ITradeDAO;

    @Autowired
    IScreenshotDAO IScreenshotDAO;

    @GetMapping("/1")
    @ResponseBody
    @Transactional
    public String atest() {


        return "test a";
    }

    @GetMapping("/upload")
    public String upload() {
        return "upload";
    }

    @GetMapping("/")
//    @ResponseBody
    public String welcome() {
        return "loginForm";
    }
}
