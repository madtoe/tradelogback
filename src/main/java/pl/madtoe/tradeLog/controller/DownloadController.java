package pl.madtoe.tradeLog.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.madtoe.tradeLog.dao.IScreenshotDAO;
import pl.madtoe.tradeLog.dao.ITradeDAO;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.*;
import java.net.URLConnection;
import java.nio.charset.Charset;

@Controller
@RequestMapping("/tradingSession/download")


public class DownloadController {

    @Autowired
    ITradeDAO tradeDAO;

    @Autowired
    IScreenshotDAO screenshotDAO;

    @Autowired
    Environment env;


    //todo pobierac sciezke screena z bazy po id

    @GetMapping("/{id}/{name}/")
    @Transactional
    public void downloadFile(HttpServletResponse response, @PathVariable("name") String filename, @PathVariable("id") String tradingSessionId) throws IOException {


        File file = null;
        ClassLoader classloader = Thread.currentThread().getContextClassLoader(); // todo: czy to potrzebne???
        file = new File(env.getProperty("uploadfolder") + "/" + tradingSessionId + "/" + filename);


        if (!file.exists()) {
            String errorMessage = "Sorry. The file you are looking for does not exist";
            System.out.println(errorMessage);
            OutputStream outputStream = response.getOutputStream();
            outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
            outputStream.close();
            return;
        }

        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        if (mimeType == null) {
            System.out.println("mimetype is not detectable, will take default");
            mimeType = "application/octet-stream";
        }

        System.out.println("mimetype : " + mimeType);

        response.setContentType(mimeType);

        /* "Content-Disposition : inline" will show viewable types [like images/text/pdf/anything viewable by browser] right on browser
            while others(zip e.g) will be directly downloaded [may provide save as popup, based on your browser setting.]*/
        response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));


        /* "Content-Disposition : attachment" will be directly download, may provide save as popup, based on your browser setting*/
        //response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));

        response.setContentLength((int) file.length());

        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

        //Copy bytes from source to destination(outputstream in this example), closes both streams.
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }


}
