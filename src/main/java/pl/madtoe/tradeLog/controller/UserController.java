package pl.madtoe.tradeLog.controller;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import pl.madtoe.tradeLog.model.TradeLogUser;
import pl.madtoe.tradeLog.model.TradingSession;
import pl.madtoe.tradeLog.service.UserService;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;



    @GetMapping("/add")
    public String addUserForm() {
        return "adduser";
    }

    @PostMapping("/new")
    @Transactional
    public RedirectView addUser(
            @RequestParam("name") String name,
            @RequestParam("password") String password,
            @RequestParam("email") String email,
            @RequestParam("rolename") String rolename,
            ModelMap model
    ) {
        userService.addUser(name, password, email, rolename);
        return new RedirectView("../tradingSession/newTradingSession");
    }


    @GetMapping("/login")
    public String loginForm() {
        return "loginForm";
    }

    @GetMapping("/loginproblem")
    @ResponseBody
    public String loginProblem() {
        return "loginproblem";
    }

    @GetMapping("/toOverview")
    @Transactional
    public RedirectView toOverview(Principal principal, RedirectAttributes attributes) {
        TradeLogUser loggedUser = userService.findLoggedUser(principal);
        if (null != loggedUser) {
            return new RedirectView("/user/" + loggedUser.getId() + "/overview");
        }
        else return new RedirectView("/loginproblem");
    }

    @GetMapping("/{id:[0-9]}/overview")
    @Transactional
    public String userOverview(@PathVariable("id") long id, ModelMap modelMap, Principal principal) {
        Long loggedID = userService.findLoggedUser(principal).getId();
        if (loggedID == id) {
            List<TradingSession> userSessions = userService.findUserSessions(principal);


            modelMap.addAttribute("userSessions", userSessions);
            modelMap.addAttribute("users", userService.getTradeLogUsers());
            return "userOverview";

        }
            return "loginForm";
    }
}
