package pl.madtoe.tradeLog.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import pl.madtoe.tradeLog.dao.IScreenshotDAO;
import pl.madtoe.tradeLog.dao.ITradingSessionDAO;
import pl.madtoe.tradeLog.dao.IUserDAO;
import pl.madtoe.tradeLog.model.TradingSession;
import pl.madtoe.tradeLog.service.TradingSessionService;
import pl.madtoe.tradeLog.service.UserService;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RequestMapping("/tradingSession")
@Controller
public class TradingSessionController {

    @Autowired
    ITradingSessionDAO tradingSessionDAO;
    @Autowired
    IScreenshotDAO screenshotDAO;

    @Autowired
    Environment env;

    @Autowired
    IUserDAO userDAO;
    @Autowired
    TradingSessionService tradingSessionService;

    @Autowired
    UserService userService;

    @GetMapping("/newTradingSession")
    @ResponseBody
    @Transactional
    public RedirectView newTradingSession(RedirectAttributes attributes, Principal principal) {
        TradingSession tradingSession = tradingSessionService.createSession(principal);
        return new RedirectView("/tradingSession/" + tradingSession.getId());
    }

    @Transactional
    @GetMapping("/{id:[0-9]+}")

//    @ResponseBody
    public String showTradingSession(@PathVariable("id") Long id, ModelMap modelMap, Principal principal) {
        Long loggedID = userService.findLoggedUser(principal).getId();
        if (loggedID == tradingSessionDAO.findOne(id).getUserId()) {

            List<TradingSession> userSessions = userService.findUserSessions(principal);
            Optional<TradingSession> tradingSession = tradingSessionDAO.findFirstById(id);
            TradingSession tradingSession1;
            if (tradingSession.isPresent()) {
                tradingSession1 = tradingSession.get();
                modelMap.addAttribute("userSessions", userSessions);
                modelMap.addAttribute("users", userService.getTradeLogUsers());
                modelMap.addAttribute("screenshots", tradingSession1.getScreenshotList());
                modelMap.addAttribute("tradingSessionId", id);
                return "viewTradingSession";
            }
            return "noSession";
        }
        return "loginForm";
    }


    @PostMapping("/{id:[0-9]+}/uploadFile")
    @Transactional
    public String submit(@PathVariable("id") Long id, @RequestParam("file") MultipartFile screenshot, ModelMap
            modelMap) {
        if (screenshot.isEmpty()) {
            modelMap.addAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }
        tradingSessionService.uploadScreenshot(id, screenshot);
        modelMap.addAttribute("file", screenshot);
//        modelMap.addAttribute("scr", scr);
        modelMap.addAttribute("tradingSessionId", id);
        modelMap.addAttribute("screenshots", tradingSessionDAO.findOne(id).getScreenshotList());
        return "viewTradingSession";
    }
}
